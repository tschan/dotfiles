#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias cat='bat'

alias ls='ls --color=auto'
alias ll='ls -l --color=auto'
alias la='ls -la --color=auto'

alias repoctl-aur='REPOCTL_CONFIG=~/.config/repoctl/aur.toml repoctl'
alias repoctl-aur-git='REPOCTL_CONFIG=~/.config/repoctl/aur-git.toml repoctl'
# to allow management of the etc git repo via sudo this alias ensures that the ssh command
# used by git will use the ssh config of the user executing the sudo command. By default
# it would use the ssh config of the root user. This is needed as the git origin remote
# of the repo is configured in the user ssh config
alias etcgit='sudo -E GIT_SSH_COMMAND="ssh -F ~/.ssh/config" git'
# the next alias opens a new terminal instance in the current working directory and completely
# disconnects it from the opening shell
alias twd='alacritty --working-directory "$(pwd)" </dev/null >/dev/null 2>&1 & disown'

# we want to source the git-prompt script to have the __git_ps1 function available
source /usr/share/git/completion/git-prompt.sh

GIT_PS1_SHOWDIRTYSTATE=1

PS1='\[\033[38;5;012m\]┌─[\[\033[01;32m\]\u@\h\[\033[38;5;012m\]]$(__git_ps1 "[\[\033[38;5;3m\]%s\[\033[38;5;012m\]]")[\[\033[38;5;1m\]\w\[\033[38;5;012m\]]\n\[\033[38;5;012m\]└─$([ $? == 0 ] && echo "\[\033[38;5;5m\]" || echo "\[\033[38;5;1m\]")\$ \[\e[0m\]'

# most of the following options were taken from:
# https://github.com/mrzool/bash-sensible/blob/master/sensible.bash

## GENERAL OPTIONS ##
# Update window size after every command
shopt -s checkwinsize
# Enable history expansion with space
# E.g. typing !!<space> will replace the !! with your last command
bind Space:magic-space
# Turn on recursive globbing (enables ** to recurse all directories)
shopt -s globstar 2> /dev/null
# Typing a directory name just by itself will automatically change into that directory
shopt -s autocd

## HISTORY RELATED STUFF
# store the last 1000000 commands in the history file
HISTFILESIZE=1000000
# load the last 100000 commands into memory
HISTSIZE=100000
# ignore consecutive duplicate commands and commands that start with space, also try
# to make sure that there is only one instance of the given command in the history
HISTCONTROL="erasedups:ignoreboth"
# force multiline commands to be in one line in history file
shopt -s cmdhist
# append to history file instead of overwriting it
shopt -s histappend
# record each line as it gets issued
PROMPT_COMMAND="history -a; $PROMPT_COMMAND"
# don't record some commands
HISTIGNORE="exit:exit :history:history :clear:clear "
# Enable incremental history search with up/down arrows (also Readline goodness)
# Learn more about this here: http://codeinthehole.com/writing/the-most-important-command-line-tip-incremental-history-searching-with-inputrc/
bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'
bind '"\e[C": forward-char'
bind '"\e[D": backward-char'

## SMARTER TAB-COMPLETION (Readline bindings) ##
# The TAB key cycles forward through the completion choices
bind "TAB: menu-complete"
# The Shift-TAB key cycles backward through the completion choices
bind '"\e[Z": menu-complete-backward'
# The first press of the completion key, TAB, will display a list of choices
# that match the given prefix, whilst the next press of the completion key
# will start cycling through the available choices
bind "set menu-complete-display-prefix on"
# Enable colors when completing filenames and directories.
bind "set colored-stats on"
# When a completion matches multiple items highlight the common matching prefix
# in color.
bind "set colored-completion-prefix on"
# Perform file completion in a case insensitive fashion
bind "set completion-ignore-case on"
# Treat hyphens and underscores as equivalent
bind "set completion-map-case on"
# Display matches for ambiguous patterns at first tab press
bind "set show-all-if-ambiguous on"
# Immediately add a trailing slash when autocompleting symlinks to directories
bind "set mark-symlinked-directories on"
# Disable beeps
bind "set bell-style none"

## fzf related completion and keybindings
source /usr/share/fzf/completion.bash
source /usr/share/fzf/key-bindings.bash

# setup ssh-agent
if ! pgrep -u "$USER" ssh-agent > /dev/null; then
    ssh-agent > "$XDG_RUNTIME_DIR/ssh-agent.env"
fi
if [[ ! -f "$SSH_AUTH_SOCK" ]]; then
    source "$XDG_RUNTIME_DIR/ssh-agent.env" >/dev/null
fi

