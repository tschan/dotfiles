#!/usr/bin/env bash

save_location="$1"

type_choices=("selection" "window" "fullscreen")
save_choices=("clipboard" "file" "both")

command=("menyoki" "-q" "capture")

case "$(printf '%s\n' "${type_choices[@]}" | rofi -dmenu -p "Screenshot > ")" in
    "fullscreen")
        command+=("--root")
        ;;
    "selection")
        command+=("--root" "--size" "\$(slop)")
        ;;
    "window")
        command+=("--focus")
        ;;
    *)
        exit
        ;;
esac

clipboard_command='xclip -selection c -t image/png'
file_command="tee '$save_location/screenshot-$(date +%Y%m%d-%H%M%S).png'"
command+=("png" "save" "-")

case "$(printf '%s\n' "${save_choices[@]}" | rofi -dmenu -p "Save to > ")" in
    "file")
        command+=("|" "$file_command")
        ;;
    "clipboard")
        command+=("|" "$clipboard_command")
        ;;
    "both")
        command+=("|" "$clipboard_command" "|" "$file_command")
        ;;
    *)
        exit
        ;;
esac

echo "Executing command: ${command[*]}"

# necessary to give the rofi window time to disappear before taking the screenshot
sleep 0.1

eval "${command[*]}"

