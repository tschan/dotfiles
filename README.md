# Installation

After cloning execute the `install` script to create all necessary symlinks.

# Post installation

After cloning there are several files that will most likely need to be adjusted in small ways:

- `git/gitconfig`:
  - Change the committer name and email
- `redshift/redshift.conf`
  - Add your location in the `[manual]` section of the config file

Ideally you would create a local branch and commit those changes there. Then after pulling the most recent changes just rebase this branch on top of `master`.

